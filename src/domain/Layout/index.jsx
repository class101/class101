import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './style.scss'

export const Layout = ({ children }) => (
    <div id="main-layout">

        <header>
            <h2>테스트몰</h2>
            <ul>
                <li>
                    <Link to="/products">상품</Link>
                </li>
                <li>
                    <Link to="/cart">카트</Link>
                </li>
            </ul>
        </header>

        <main>
            {children}
        </main>

        <footer>
            Author : Sebastien Mesili - sebastien@mesili.fr 
        </footer>

    </div>
)

Layout.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.node)
    ]).isRequired
}

export default Layout
