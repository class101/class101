### Details 

- State management : Redux ( + redux-thunk) 
- Routing : react-router-dom 
- Theming : 
 - icons : react-icons (font-awesome) 
 - css : node-sass (scss) 


### Routes

 - `/cart` 
 - `/products` 

### src Folder structure

```
/components      common components with no side effect
/data            provided data
/domain          domain-specific folders
  /Products        Products related components and state
  /Cart            Cart related components and state
/helpers         various helpers functions
/store           redux store
/theme           global scss files

```
